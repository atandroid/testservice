package haudo.com.serviceatgroup;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by HauDo on 3/10/2018.
 */

public class TestService extends Service {

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent sent = new Intent("GPSLocationUpdates");
        sent.putExtra("Status", "Data from service");
        LocalBroadcastManager.getInstance(this).sendBroadcast(sent);
        return START_REDELIVER_INTENT;
    }
}