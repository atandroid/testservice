package haudo.com.serviceatgroup;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TestBindService testBindService;
    Intent intent = new Intent();

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            testBindService = ((TestBindService.MyBinder) service).getService(MainActivity.this);
            testBindService.numStar = 10;
            startService(intent);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            testBindService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //localBroadCastReceiver();
        //testBindServicexx();

        intentService();
    }

    private void intentService() {
        startService(new Intent(getBaseContext(), TestIntentService.class));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("DataFromIntentService"));
    }

    private void testBindServicexx() {
        intent = new Intent(MainActivity.this, TestBindService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("DataFromBindService"));
    }

    private void localBroadCastReceiver() {
        startService(new Intent(this, TestService.class));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            int message = intent.getIntExtra("Status", 0);
            Log.e("logg", message + "");
            Toast.makeText(context, "Count " + message, Toast.LENGTH_SHORT).show();
        }
    };

    /*private BroadcastReceiver messageFromBind = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int message = intent.getIntExtra("Status", 0);
            Log.e("logg", message + "");
            Toast.makeText(context, "Count " + message, Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver messageFromIntent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int message = intent.getIntExtra("Status", 0);
            Log.e("logg", message + "");
            Toast.makeText(context, "Count " + message, Toast.LENGTH_SHORT).show();
        }
    };*/

}
