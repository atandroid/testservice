package haudo.com.serviceatgroup;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by HauDo on 3/10/2018.
 */

public class TestBindService extends Service {

    Context context;
    int numStar = 0;


    public class MyBinder extends Binder {
        TestBindService getService(Context con) {
            context = con;
            return TestBindService.this;
        }
    }

    private final IBinder binder = new MyBinder();

    @Override
    public IBinder onBind(Intent arg0) {

        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        new DoBackgroundTask().execute(numStar);

        return super.onStartCommand(intent, flags, startId);
    }

    class DoBackgroundTask extends AsyncTask<Integer, Integer, Integer> {

        int num = 0;

        @Override
        protected Integer doInBackground(Integer... integers) {
            while (num < integers[0]) {
                try {
                    num++;
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.e("logg", "service " + num);
            return num;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
            Intent sent = new Intent("DataFromBindService");
            sent.putExtra("Status", num);
            Log.e("logg", "service " + num);
            LocalBroadcastManager.getInstance(context).sendBroadcast(sent);
            stopSelf();
        }
    }
}
