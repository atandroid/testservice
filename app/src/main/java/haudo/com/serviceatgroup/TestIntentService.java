package haudo.com.serviceatgroup;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by HauDo on 3/10/2018.
 */

public class TestIntentService extends IntentService {

    Context context;

    public TestIntentService(){
        super("This is service");
    }

    public TestIntentService(Context context) {
        super("This is service");
        this.context = context;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int result = CountToMillion(999990);
        Intent sent = new Intent("DataFromIntentService");
        sent.putExtra("Status", result);
        Log.e("logg", "service " + result);
        LocalBroadcastManager.getInstance(context).sendBroadcast(sent);
    }

    private int CountToMillion(int numStar) {
        try {
            while (numStar < 1000000) {
                numStar++;
                Thread.sleep(500);
                Log.e("logg", numStar + "");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return numStar;
    }
}
